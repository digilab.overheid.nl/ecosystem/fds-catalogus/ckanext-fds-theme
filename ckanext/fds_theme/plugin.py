import logging
import pprint

from flask import Blueprint

import ckan.plugins as plugins
import ckan.plugins.toolkit as toolkit

import ckanext.fds_theme.views.dataservice as dataservice

log = logging.getLogger(__name__)


class FdsThemePlugin(plugins.SingletonPlugin):
    plugins.implements(plugins.IBlueprint)
    plugins.implements(plugins.IConfigurer)


    # IConfigurer
    def update_config(self, config_):
        toolkit.add_template_directory(config_, "templates")
        toolkit.add_public_directory(config_, "public")
        toolkit.add_resource("assets", "fds_theme")


    # IBlueprint
    def get_blueprint(self):
        blueprint = Blueprint(self.name, self.__module__,
            url_prefix='/dataservice',
            url_defaults={
                'package_type': 'dataset',
            },
        )
        blueprint.add_url_rule("/",
            view_func=dataservice.search,
            endpoint="dataservice_search",
        )
        return [blueprint]
